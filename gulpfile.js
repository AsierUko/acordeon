
var     gulp         = require('gulp'),
        sass         = require('gulp-sass'),
        watch        = require('gulp-watch'),
        browserSync  = require('browser-sync').create(),
        sourcemaps   = require('gulp-sourcemaps'),
        autoprefixer = require('gulp-autoprefixer'),
        webpack      = require('webpack'),
				imagemin     = require('gulp-imagemin'),
				del			 		 = require('del'),
				usemin		 	 = require('gulp-usemin'),
				rev 		 		 = require('gulp-rev'),
				cssnano			 = require('gulp-cssnano'),
				uglify			 = require('gulp-uglify');

// GULP-SASS COMPILER
gulp.task('sass', function () {
  return gulp.src('./app/assets/styles/sass/style.scss')
    .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
     }))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./app/assets/temp/'));
});

// CSS Inject
gulp.task("cssInject", ['sass'], function(){
    return gulp.src("./app/assets/temp/style.css")
        .pipe(browserSync.stream());
});

// WEBPACK
gulp.task('scripts', function(callback){

	return gulp.src('./app/assets/scripts/scripts.js')
		.pipe(gulp.dest('./app/assets/temp/'));
});

// REFRESH SCRIPTS
gulp.task('scriptsRefresh',['scripts'], function(){
	browserSync.reload();
});

//MAIN TASK
gulp.task("watch", function(){

    browserSync.init({
		host: "10.24.14.77",
			notify: false,
			server:{
					baseDir: "app"
			}
    });

    watch("./app/*.html", function(){
       browserSync.reload();
    });

    watch('./app/assets/styles/sass/**/*.scss', function(){
        gulp.start("cssInject");
    });

	watch('./app/assets/scripts/**/*.js', function(){
		gulp.start('scriptsRefresh');
	});
});

// GENERATE DIST DIRECTORY **********************************************************************************

//DELETE DIST FOLDER
gulp.task('deleteDistFolder', function(){
	return del('./dist');
});

//COPY GENERAL FILES
gulp.task('copyGeneralFiles', ['deleteDistFolder'], function(){
	var pathsToCopy = [
		'./app/**/*',
		'!./app/index.html',
		'!./app/assets/images/**',
		'!./app/assets/styles/**',
		'!./app/assets/scripts/**',
		'!./app/assets/temp/**',
		'!./app/assets/temp'
	];

	return gulp.src(pathsToCopy)
		.pipe(gulp.dest('./dist'));
});

//IMAGES OPTIMIZATION
gulp.task('optimizeImages', ['deleteDistFolder'], function(){
	return gulp.src('./app/assets/images/**/*')
		.pipe(imagemin({
			progressive: true,
			interlaced: true,
			multipass: true
		}))
		.pipe(gulp.dest('./dist/assets/images'));
});

//USEMIN
gulp.task('usemin', ['deleteDistFolder', 'sass', 'scripts' ], function(){
	return gulp.src('./app/*.html')
		.pipe(usemin({
			css: [function() {return rev()}, function() {return cssnano()}]
			//js: [function() {return rev()}, function() {return uglify()}]
		}))
		.pipe(gulp.dest('./dist/'));
})

//BUILD TASK
gulp.task('build', ['deleteDistFolder','copyGeneralFiles','optimizeImages', 'usemin']);
